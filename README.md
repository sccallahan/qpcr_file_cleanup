qPCR file cleanup
================
Carson Callahan

Intro
-----

This script will clean up the data from the qPCR machines we have in lab (Stratagene Mx3005P from Agilent). It's been written to run from the command line. You may need to tweak some of the code youself based on how your plate is set up (I assume a fairly normal plate setup for rows and columns). Generally, I just use this code to clean the data to make it easier to work with, then just do the ddCT calculations in Excel. The variability in plate setups makes generalizing code for that a bit difficult.

Script usage
------------

The data should be in .csv format. I generally save the results as .csv, then just manually convert that to an excel workbook so the formulas will work for ddCT calculations.

The script requires the following libraries to be installed:

-   optparse
-   tidyverse
-   data.table

These can all be retrived from CRAN using the standard `install.packages` function. This script should be placed in the same directory as the file to be cleaned, or otherwise be placed in a scripts directory that's part of your PATH. Then, execute the script as follows (you should run `chmod u+x qPCR_cleaner.R` before the first time you use the script):

``` bash
qPCR_cleaner.R -f [filename] -c [number of columns used] -r [number of rows used]
```

If there's an issue with the script actually running, you may need to prefix the line above with `Rscript`. So, that would look like this:

``` bash
Rscript qPCR_cleaner.R -f [filename] -c [number of columns used] -r [number of rows used]
```

The output will be saved to the same directory as the input file. The file is named as `input_name_processed.csv` and is saved as a csv file.

#### Other notes

If you're on a Windows system, this will function just fine in WSL. Otherwise, you'll need to load the R script itself into RStudio and run it manually.
